﻿using Day04Tasks;

namespace ListsTests;

[TestClass]
public class ExpandExpressionTests
{
    [TestMethod]
    public void TestExpressionExpansion()
    {
        //Assert
        string expression = "AB3(DC)2(F)2(E3(G))";
        string expected = "ABDCDCDCFFEGGGEGGG";

        //Act
        string result = ListsHomeWork.ExpandExpression(expression);

        //Assert
        Assert.AreEqual(expected, result);
    }

    [TestMethod]
    public void TestExpressionExpansionWithNumberFollowedByLetters()
    {
        //Assert
        string expression = "3A1B3(DC)2(0F)2(E3(G))";
        string expected = "AAABDCDCDCFFEGGGEGGG";

        //Act
        string result = ListsHomeWork.ExpandExpression(expression);

        //Assert
        Assert.AreEqual(expected, result);
    }

    [TestMethod]
    public void TestExpressionExpansionWitZerosInBrackets()
    {
        //Assert
        string expression = "AB3(DC)2(0FZZ0R0P)2(E3(G))";
        string expected = "ABDCDCDCFZZRPFZZRPEGGGEGGG";

        //Act
        string result = ListsHomeWork.ExpandExpression(expression);

        //Assert
        Assert.AreEqual(expected, result);
    }
}
