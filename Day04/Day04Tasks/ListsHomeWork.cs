﻿using System.Text;

namespace Day04Tasks;

public class ListsHomeWork
{
    public static List<T> UnifyList<T>(List<T> list)
    {
        HashSet<T> newList = new HashSet<T>();

        foreach (var item in list)
        {
            newList.Add(item);
        }

        return newList.ToList();
    }

    public static void RemoveMiddleElement<T>(LinkedList<T> list)
    {
        int middle = (list.Count / 2) + 1;
        LinkedListNode<T> node = list.First;

        for (int i = 1; i < middle; i++)
        {
            node = node.Next;
            if (i == middle - 1)
            {
                list.Remove(node);
            }
        }
    }

    public static string ExpandExpression(string expression)
    {
        Stack<int> nums = new Stack<int>();
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < expression.Length; i++)
        {
            if (expression[i] == ')')
            {
                break;
            }

            if (char.IsDigit(expression[i]))
            {
                nums.Push(expression[i++] - 48);
                if (nums.Peek() == 0)
                {
                    --i;
                    continue;
                }
                string subExpression = expression[i].ToString();
                if (expression[i] == '(')
                {
                    subExpression = ExpandExpression(expression.Substring(++i));
                    if (expression[i] - 48 == 0)
                    {
                        int j = i;
                        while (expression[j] != ')')
                        {
                            if (expression[j++] - 48 == 0)
                            {
                                ++i;
                            }
                        }
                    }
                    i += subExpression.Length;
                }

                int iterations = nums.Pop();
                for (int j = 0; j < iterations; j++)
                {
                    sb.Append(subExpression);
                }
            }
            else if (char.IsLetter(expression[i]))
            {
                sb.Append(expression[i]);
            }
        }
        return sb.ToString().Trim();
    }
}
