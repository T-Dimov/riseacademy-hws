﻿namespace Day03Tasks;

public class RootThree
{
    public static int FindRootThree(int number)
    {
        int root3 = 0;
        int pow3 = 0;
        do
        {
            root3++;
            pow3 = root3 * root3 * root3;
            if (number % pow3 > 0 && number % pow3 < root3)
            {
                throw new ArgumentException("Invalid input!");
            }
        } while (!(pow3 == number));

        return root3;
    }
}
