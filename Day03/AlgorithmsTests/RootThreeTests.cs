﻿using Day03Tasks;
namespace TestAlgorithms;

[TestClass]
public class RootThreeTests
{
    [TestMethod]
    public void TestFindRootThree()
    {
        //Assign
        int number = 1728;
        int Expected = 12;

        //Act
        int result = RootThree.FindRootThree(number);

        //Assert
        Assert.AreEqual(Expected, result);
    }

    [TestMethod]
    public void TestFindRootFourWhenInvalidInput()
    {
        //Assign
        int number = 1727;
        string Expected = "Invalid input!";

        //Assert
        Assert.ThrowsException<ArgumentException>(() => RootThree.FindRootThree(number), Expected);
    }
}
