﻿namespace AlgorithmsTest;

using Day03Tasks;
[TestClass]
public class SortingTests
{
    [TestMethod]
    public void TestSortByMaxValue()
    {
        //Assign
        int[] array = { 6, 4, 1, 3, 2, 5 };
        int[] Expected = { 1, 2, 3, 4, 5, 6 };

        //Act
        FindMissingElement.SortByMaxValue(array);

        //Assert
        CollectionAssert.AreEqual(Expected, array);
    }

    [TestMethod]
    public void TestSoring()
    {
        // Assign
        int[] array = new int[] { 30, 22, 4, 6, 99, 112 };
        int[] Expected = { 4, 6, 22, 30, 99, 112 };

        // Act
        FindMissingElement.SortByMaxValue(array);

        //Assert
        CollectionAssert.AreEqual(Expected, array);
    }

    [TestMethod]
    public void TestFibunacciSoring()
    {
        // Assign
        int[] array = new int[] { 55, 2, 1, 1, 13, 8, 5, 3, 21, 34 };
        int[] Expected = { 1, 1, 2, 3, 5, 8, 13, 21, 34, 55 };

        // Act
        FindMissingElement.SortByMaxValue(array);

        //Assert
        CollectionAssert.AreEqual(Expected, array);
    }
}
