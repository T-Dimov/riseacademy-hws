using Day03Tasks;
namespace TestAlgorithms;

[TestClass]
public class MissingElementTests
{
   
    [TestMethod]
    public void TestDiscoverMissingElement()
    {
        //Assign
        int[] array = { 4, 2, 5, 7, 3 };
        int Expected = 6;

        //Act
        int result = FindMissingElement.DiscoverMissingElement(array);

        //Assert
        Assert.AreEqual(Expected, result);
    }

    [TestMethod]
    public void TestMissingElementWithOneElement()
    {
        //Assemble
        int[] array = { 1 };
        string Expected = "Array must have more than 1 element";

        //Assert
        Assert.ThrowsException<ArgumentException>(() => FindMissingElement.DiscoverMissingElement(array), Expected);
    }

    [TestMethod]
    public void TestWhenNoElementIsMissing()
    {
        //Assign
        int[] array = { 4, 2, 5, 7, 3, 6 };
        int Expected = -1;

        //Act
        int result = FindMissingElement.DiscoverMissingElement(array);

        //Assert
        Assert.AreEqual(Expected, result);
    }

    [TestMethod]
    public void TestWhenArrayHasTwoElements()
    {
        //Assign
        int[] array = { 4, 2 };
        int Expected = 3;

        //Act
        int result = FindMissingElement.DiscoverMissingElement(array);

        //Assert
        Assert.AreEqual(Expected, result);
    }

    [TestMethod]
    public void TestWhenArrayHasZeroElements()
    {
        //Assign
        int[] array = new int[0];
        string Expected = "Array must have more than 1 element";

        //Assert
        Assert.ThrowsException<ArgumentException>(() => FindMissingElement.DiscoverMissingElement(array), Expected);
    }
    // add more tests for the sorting algorithm
}